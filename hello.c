#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* bogus commit to force compilation after resetting runner caches */

int main(void)
{
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    fprintf(stderr,"\nCompile date and timet: %s @ %s\n",__DATE__,__TIME__);
    fprintf(stderr,"Current date and time: %s\n", asctime(timeinfo));
    return 0;
}


# Variables
CC = gcc
SOURCE = hello.c
OBJDIR = obj
OBJECT = $(OBJDIR)/hello.o
EXECUTABLE = hello.exe

# Default target
all: $(EXECUTABLE)

# Link the object file to produce the executable
$(EXECUTABLE): $(OBJECT)
	$(CC) $< -o $@

# Compile the source file into an object file
$(OBJECT): $(SOURCE) | $(OBJDIR)
	$(CC) -c $< -o $@

# Create the 'obj' directory if it doesn't exist
$(OBJDIR):
	mkdir -p $@

# Clean target to remove generated files
clean:
	rm -rf $(OBJDIR) $(EXECUTABLE)

.PHONY: all clean

